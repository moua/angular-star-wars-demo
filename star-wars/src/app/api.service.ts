import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Film } from './film.model';
import { formatDate } from '@angular/common';
import { Person } from './person.model';
import { Starship } from './starship.model';
import { Vehicle } from './vehicles.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseUrl = 'https://swapi.dev/api/';
  private releaseDateFormat = 'shortDate';
  private dateFormat = 'short';
  private dateLocale = 'en-us';
  private radix = 10;

  constructor(private http: HttpClient) { }

  /**
   * Extract the id from the url.
   * @param url the url string.
   * @returns the id value from the url.
   */
  public getId(url: string): string {
    return url.match(/\d+/)[0];
  }

  /**
   * Helper method to create a film object.
   * @param data the response data for the film.
   * @returns an object for the film.
   */
  private createFilm(data: any): Film {
    return new Film({
      title: data.title,
      episodeId: data.episode_id,
      openingCrawl: data.opening_crawl,
      director: data.director,
      producer: data.producer,
      releaseDate: formatDate(data.release_date, this.releaseDateFormat, this.dateLocale),
      characters: (data.characters as string[]).map(this.getId),
      planets: (data.planets as string[]).map(this.getId),
      starships: (data.starships as string[]).map(this.getId),
      vehicles: (data.vehicles as string[]).map(this.getId),
      species: (data.species as string[]).map(this.getId),
      created: formatDate(data.created, this.dateFormat, this.dateLocale),
      edited: formatDate(data.edited, this.dateFormat, this.dateLocale),
      url: data.url
    });
  }

  /**
   * Helper method to create a person object.
   * @param data the response data for the person.
   * @returns an object for the person.
   */
  private createPerson(data: any): Person {
    return new Person({
      name: data.name,
      height: data.height,
      mass: data.mass,
      hairColor: data.hair_color,
      skinColor: data.skin_color,
      eyeColor: data.eye_color,
      birthYear: data.birth_year,
      gender: data.gender,
      homeworld: this.getId(data.homeworld),
      films: (data.films as string[]).map(this.getId),
      species: (data.species as string[]).map(this.getId),
      starships: (data.starships as string[]).map(this.getId),
      vehicles: (data.vehicles as string[]).map(this.getId),
      created: formatDate(data.created, 'short', 'en-us'),
      edited: formatDate(data.edited, 'short', 'en-us'),
      url: data.url
    });
  }

  /**
   * Helper method to create a starship object.
   * @param data the response data for the starship.
   * @returns an object for the starship.
   */
  public createStarship(data: any): Starship {
    return new Starship({
      name: data.name,
      model: data.model,
      manufacturer: data.manufacturer,
      costInCredits: data.cost_in_credits,
      length: data.length,
      maxAtmospheringSpeed: data.max_atmosphering_speed,
      // Handle the case where the value given is a range (e.g. 30-160), take the first value to be safe.
      crew: parseInt(data.crew.split('-')[0], this.radix),
      // Handle the case where the value given is not a number (e.g. "n/a"), return a zero.
      passengers: isNaN(parseInt(data.passengers, this.radix)) ? 0 : parseInt(data.passengers, this.radix),
      cargoCapacity: data.cargo_capacity,
      consumables: data.consumables,
      hyperdriveRating: data.hyperdrive_rating,
      mglt: data.MGLT,
      starshipClass: data.starship_class,
      pilots: data.pilots,
      films: (data.films as string[]).map(this.getId),
      created: formatDate(data.created, 'short', 'en-us'),
      edited: formatDate(data.edited, 'short', 'en-us'),
      url: data.url,
    });
  }

  /**
   * Helper method to create a vehicle object.
   * @param data the response data for the vehicle.
   * @returns an object for the vehicle.
   */
   public createVehicle(data: any): Vehicle {
    return new Vehicle({
      name: data.name,
      model: data.model,
      manufacturer: data.manufacturer,
      costInCredits: data.cost_in_credits,
      length: data.length,
      maxAtmospheringSpeed: data.max_atmosphering_speed,
      // Handle the case where the value given is a range (e.g. 30-160), take the first value to be safe.
      crew: parseInt(data.crew.split('-')[0], this.radix),
      // Handle the case where the value given is not a number (e.g. "n/a"), return a zero.
      passengers: isNaN(parseInt(data.passengers, this.radix)) ? 0 : parseInt(data.passengers, this.radix),
      cargoCapacity: data.cargo_capacity,
      consumables: data.consumables,
      vehicleClass: data.vehicle_class,
      pilots: data.pilots,
      films: (data.films as string[]).map(this.getId),
      created: formatDate(data.created, 'short', 'en-us'),
      edited: formatDate(data.edited, 'short', 'en-us'),
      url: data.url,
    });
  }

  /**
   * Gets the list of Star Wars films.
   * @returns an observable array of the films.
   */
  public getFilms(): Observable<Film[]> {
    const url = `${this.baseUrl}/films/`;
    return this.http.get(url).pipe(map((res: any) => {
      return (res.results as string[]).map((result) => this.createFilm(result));
    }));
  }

  /**
   * Get the all the characters.
   * @returns an observable array of all the characters.
   */
  public getPeople(): Observable<Person[]> {
    const url = `${this.baseUrl}/people/`;
    return this.http.get(url).pipe(map((res: any) => {
      return (res.results as string[]).map((result) => this.createPerson(result));
    }));
  }

  /**
   * Get the person for a specific id.
   * @param id the unique identifier.
   * @returns an observable object.
   */
  public getPersonById(id: string): Observable<Person> {
    const url = `${this.baseUrl}/people/${id}/`;
    return this.http.get(url).pipe(map((res: any) => this.createPerson(res)));
  }

  /**
   * Get the all the starships.
   * @returns an observable array of all the starships.
   */
  public getStarships(): Observable<Starship[]> {
    const url = `${this.baseUrl}/starships/`;
    return this.http.get(url).pipe(map((res: any) => {
      return (res.results as string[]).map((result) => this.createStarship(result));
    }));
  }

  /**
   * Get the starship for a specific id.
   * @param id the unique identifier.
   * @returns an observable object.
   */
   public getStarshipById(id: string): Observable<Starship> {
    const url = `${this.baseUrl}/starships/${id}/`;
    return this.http.get(url).pipe(map((res: any) => this.createStarship(res)));
  }

  /**
   * Get the all the vehicles.
   * @returns an observable array of all the vehicles.
   */
   public getVehicles(): Observable<Vehicle[]> {
    const url = `${this.baseUrl}/vehicles/`;
    return this.http.get(url).pipe(map((res: any) => {
      return (res.results as string[]).map((result) => this.createVehicle(result));
    }));
  }

  /**
   * Get the vehicle for a specific id.
   * @param id the unique identifier.
   * @returns an observable object.
   */
   public getVehicleById(id: string): Observable<Vehicle> {
    const url = `${this.baseUrl}/vehicles/${id}/`;
    return this.http.get(url).pipe(map((res: any) => this.createVehicle(res)));
  }

  /**
   * Searches the data base for all matches for films, people, starships and vehicles.
   * @param input the input text to search for.
   * @returns an observable array of array of data.
   */
  public getSearch(input: string): Observable<any[]> {
    const filmsUrl = `${this.baseUrl}/films/?search=${input}`;
    const peopleUrl = `${this.baseUrl}/people/?search=${input}`;
    const starshipsUrl = `${this.baseUrl}/starships/?search=${input}`;
    const vehiclesUrl = `${this.baseUrl}/vehicles/?search=${input}`;

    return forkJoin([
      this.http.get(filmsUrl).pipe(map((res: {results: any[]}) => res.results)),
      this.http.get(peopleUrl).pipe(map((res: {results: any[]}) => res.results)),
      this.http.get(starshipsUrl).pipe(map((res: {results: any[]}) => res.results)),
      this.http.get(vehiclesUrl).pipe(map((res: {results: any[]}) => res.results)),
    ]);
  }
}

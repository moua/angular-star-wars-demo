import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CharacterDetialsComponent } from './character-detials.component';

describe('CharacterDetialsComponent', () => {
  let component: CharacterDetialsComponent;
  let fixture: ComponentFixture<CharacterDetialsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterDetialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterDetialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

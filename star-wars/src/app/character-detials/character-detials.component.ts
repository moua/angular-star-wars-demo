import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Person } from '../person.model';

@Component({
  selector: 'app-character-detials',
  templateUrl: './character-detials.component.html',
  styleUrls: ['./character-detials.component.scss']
})
export class CharacterDetialsComponent implements OnInit {
  public person$: Observable<Person>;
  public bmi: number;
  public obj = Object;
  public labels = {
    height: 'Height',
    mass: 'Mass',
    hairColor: 'Hair Color',
    skinColor: 'Skin Color',
    eyeColor: 'Eye Color',
    birthYear: 'Birth Year',
    gender: 'Gender',
    homeworld: 'Home World',
    films: 'Films',
    species: 'Species',
    starships: 'Starships',
    vehicles: 'Vehicles',
    created: 'Date Created',
    edited: 'Date Edited',
    url: 'URL',
  };

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.person$ = this.route.data.pipe(map((data) => data.person));
  }
}

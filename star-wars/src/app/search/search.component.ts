import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public results$: Observable<any[]>;
  public loading = false;
  public searchForm = new FormGroup({
    searchTerm: new FormControl(),
  });

  constructor(private api: ApiService) { }

  ngOnInit(): void { }

  /**
   * Sarches the data base.
   */
  public search(): void {
    this.loading = true;
    this.results$ = this.api.getSearch(this.searchForm.controls.searchTerm.value).pipe(
      tap(() => this.loading = false),
      map((responses) => [...responses[0], ...responses[1], ...responses[2], ...responses[3]]),
    );
  }

  /**
   * Wrapper method used by the view to stringfy a JSON object.
   * @param input the JSON object to stringfy.
   * @returns an equilavent string version of the JSON object.
   */
  public jsonIt(input: string): string {
    return JSON.stringify(input, undefined, 4);
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { Film } from './film.model';
import { Person } from './person.model';
import { Starship } from './starship.model';
import { Vehicle } from './vehicles.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  /**
   * Filters through an array of objects.
   * @param searchable the array to search from.
   * @param value the intended value to searc for.
   * @returns the filtered array matching the value.
   */
  transform(searchable: any[], value: string): any[] {
    if (!value) {
      return searchable;
    }

    return searchable.filter((item) => {
      if (item instanceof Film) {
        return item.title.toLocaleLowerCase().includes(value.toLocaleLowerCase());
      } else if (item instanceof Person) {
        return item.name.toLocaleLowerCase().includes(value.toLocaleLowerCase());
      } else if (item instanceof Starship || item instanceof Vehicle) {
        return item.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())
        || item.model.toLocaleLowerCase().includes(value.toLocaleLowerCase());
      }
    });
  }
}

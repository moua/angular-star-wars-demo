import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Starship } from '../starship.model';

@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.scss']
})
export class StarshipsComponent implements OnInit {
  public starships$: Observable<Starship[]>;
  public columnsHeaders = {
    name: 'Name',
    model: 'Model',
    manufacturer: 'Manufacturer',
    starshipClass: 'Class',
    url: 'Details',
  };
  public searchForm = new FormGroup({
    searchTerm: new FormControl(),
  });

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.starships$ = this.route.data.pipe(map((data) => data.starships));
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() dataSource$: Observable<any>;
  @Input() searchValue: string;
  @Input() columnsHeaders: Record<string, unknown>;
  @Input() routeUrl: string;

  public obj = Object;

  constructor(private api: ApiService) { }

  ngOnInit(): void { }

  /**
   * Extract the id from the url.
   * @param url the url string.
   * @returns the id value from the url.
   */
   public getId(url: string): string {
    return this.api.getId(url);
  }

  /**
   * Task for returning the index to be tracked.
   * @param index the index of the person.
   * @returns the index of the person.
   */
   public trackIndexId(index: number): number {
    return index;
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterDetailsResolver } from './resolvers/character-details.resolver';
import { CharacterDetialsComponent } from './character-detials/character-detials.component';
import { CharactersComponent } from './characters/characters.component';
import { FilmsComponent } from './films/films.component';
import { SearchComponent } from './search/search.component';
import { StarshipDetailsResolver } from './resolvers/starship-details.resolver';
import { StarshipDetailsComponent } from './starship-details/starship-details.component';
import { StarshipsComponent } from './starships/starships.component';
import { FilmsResolver } from './resolvers/films.resolver';
import { CharactersResolver } from './resolvers/characters.resolver';
import { StarshipsResolver } from './resolvers/starships.resolver';

const routes: Routes = [
  {
    path: 'films',
    component: FilmsComponent,
    data: { title: 'Films'},
    resolve: { films: FilmsResolver },
  },
  {
    path: 'characters',
    component: CharactersComponent,
    data: { title: 'Characters'},
    resolve: { people: CharactersResolver },
  },
  {
    path: 'characters/character-details/:id',
    component: CharacterDetialsComponent,
    data: { title: 'Character Details'},
    resolve: { person: CharacterDetailsResolver }
  },
  {
    path: 'starships',
    component: StarshipsComponent,
    data: { title: 'Starships'},
    resolve: { starships: StarshipsResolver },
  },
  {
    path: 'starships/starship-details/:id',
    component: StarshipDetailsComponent,
    data: { title: 'Starship Details'},
    resolve: { starship: StarshipDetailsResolver },
  },
  {
    path: 'search',
    component: SearchComponent,
    data: { title: 'Search'}
  },
  // {
  //   path: 'people',
  //   loadChildren: () => import('./people/people.module').then((m) => m.PeopleModule),
  // },
  {
    path: 'automobile',
    loadChildren: () => import('./automobile/automobile.module').then((m) => m.AutomobileModule),
  },
  {
    path: '',
    pathMatch: 'full', /* The path must match if completely. */
    redirectTo: 'films',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from '../table/table.component';
import { MatTableModule } from '@angular/material/table';
import { FilterPipe } from '../filter.pipe';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [TableComponent, FilterPipe],
  imports: [
    CommonModule,
    MatTableModule,
    RouterModule,
  ],
  providers: [],
  exports: [TableComponent]
})
export class SharedModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Starship } from '../starship.model';

@Component({
  selector: 'app-starship-details',
  templateUrl: './starship-details.component.html',
  styleUrls: ['./starship-details.component.scss']
})
export class StarshipDetailsComponent implements OnInit {
  public starship$: Observable<Starship>;
  public obj = Object;
  public labels = {
    name: 'Name',
    model: 'Model',
    manufacturer: 'Manufacturer',
    costInCredits: 'Cost In Credits',
    length: 'Length',
    maxAtmospheringSpeed: 'Max Atmosphering Speed',
    crew: 'Crew',
    passengers: 'Passengers',
    cargoCapacity: 'Cargo Capacity',
    consumables: 'Comsumables',
    hyperdriveRating: 'Hyper Drive Rating',
    mglt: 'MGLT',
    starshipClass: 'Starship Class',
    pilots: 'Pilots',
    films: 'Films',
    created: 'Created',
    edited: 'Edited',
    url: 'URL',
  };

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.starship$ = this.route.data.pipe(map((data) => data.starship));
  }
}

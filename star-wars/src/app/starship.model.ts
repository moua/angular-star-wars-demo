export class Starship {
  constructor(attrs: Record<string, unknown>) {
    Object.assign(this, attrs);
  }
  name: string;
  model: string;
  manufacturer: string;
  costInCredits: string;
  length: string;
  maxAtmospheringSpeed: string;
  crew: number;
  passengers: number;
  cargoCapacity: string;
  consumables: string;
  hyperdriveRating: string;
  mglt: string;
  starshipClass: string;
  pilots: string[];
  films: string[];
  created: string;
  edited: string;
  url: string;
}

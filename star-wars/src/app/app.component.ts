import { OnDestroy, OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private routerEvent: Subscription;
  private routeData: Subscription;
  public loading: boolean;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private title: Title) { }

  ngOnInit(): void {
    // Listen for route change events and set the browser tab title.
    this.routerEvent = this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.loading = true;
      } else if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
        this.loading = false;
        // Set the browser title with the route title.
        if (event instanceof NavigationEnd) {
          // Get the very last route in the router tree.
          const route = this.getLastChildRoute(this.activatedRoute);
          this.routeData = route.data.subscribe((data) => this.title.setTitle(`${data.title} | STAR WARS`));
        }
      }
    });
  }

  public getLastChildRoute(route: ActivatedRoute): ActivatedRoute | null {
    let childRoute = route.firstChild;
    while (childRoute.firstChild) {
        childRoute = childRoute.firstChild;
    }
    return childRoute;
  }

  ngOnDestroy(): void {
    this.routerEvent.unsubscribe();
    this.routeData.unsubscribe();
  }
}

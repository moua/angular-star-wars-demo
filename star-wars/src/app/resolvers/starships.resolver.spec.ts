import { TestBed } from '@angular/core/testing';

import { StarshipsResolver } from './starships.resolver';

describe('StarshipsResolver', () => {
  let resolver: StarshipsResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(StarshipsResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Vehicle } from '../vehicles.model';

@Injectable({
  providedIn: 'root'
})
export class VehicleDetailsResolver implements Resolve<Observable<Vehicle>> {
  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Vehicle> {
    return this.api.getVehicleById(route.paramMap.get('id'));
  }
}

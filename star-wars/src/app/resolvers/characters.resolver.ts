import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Person } from '../person.model';

@Injectable({
  providedIn: 'root'
})
export class CharactersResolver implements Resolve<Observable<Person[]>> {
  constructor(private api: ApiService) { }

  resolve(): Observable<Person[]> {
    return this.api.getPeople();
  }
}

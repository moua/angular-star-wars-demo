import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Starship } from '../starship.model';

@Injectable({
  providedIn: 'root'
})
export class StarshipsResolver implements Resolve<Observable<Starship[]>> {
  constructor(private api: ApiService) { }

  resolve(): Observable<Starship[]> {
    return this.api.getStarships();
  }
}

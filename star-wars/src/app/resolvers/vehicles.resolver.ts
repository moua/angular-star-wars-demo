import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Vehicle } from '../vehicles.model';

@Injectable({
  providedIn: 'root'
})
export class VehiclesResolver implements Resolve<Observable<Vehicle[]>> {
  constructor(private api: ApiService) { }

  resolve(): Observable<Vehicle[]> {
    return this.api.getVehicles();
  }
}

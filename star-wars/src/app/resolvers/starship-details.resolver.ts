import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Starship } from '../starship.model';

@Injectable({
  providedIn: 'root'
})
export class StarshipDetailsResolver implements Resolve<Observable<Starship>> {
  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Starship> {
    return this.api.getStarshipById(route.paramMap.get('id'));
  }
}

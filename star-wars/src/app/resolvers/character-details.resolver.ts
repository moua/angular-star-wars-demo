import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Person } from '../person.model';

@Injectable({
  providedIn: 'root'
})
export class CharacterDetailsResolver implements Resolve<Observable<Person>> {
  constructor(private api: ApiService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Person> {
    return this.api.getPersonById(route.paramMap.get('id'));
  }
}

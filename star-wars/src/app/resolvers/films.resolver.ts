import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Film } from '../film.model';

@Injectable({
  providedIn: 'root'
})
export class FilmsResolver implements Resolve<Observable<Film[]>> {
  constructor(private api: ApiService) { }

  resolve(): Observable<Film[]> {
    return this.api.getFilms();
  }
}

import { TestBed } from '@angular/core/testing';

import { StarshipDetailsResolver } from './starship-details.resolver';

describe('StarshipDetailsResolver', () => {
  let resolver: StarshipDetailsResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(StarshipDetailsResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { VehicleDetailsResolver } from './vehicle-details.resolver';

describe('VehicleDetailsResolver', () => {
  let resolver: VehicleDetailsResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(VehicleDetailsResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});

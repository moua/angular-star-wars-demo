import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Person } from '../person.model';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
  public people$: Observable<Person[]>;
  public columnsHeaders = {
    name: 'Name',
    height: 'Height',
    mass: 'Mass',
    birthYear: 'Birth Year',
    url: 'Details',
  };
  public searchForm = new FormGroup({
    searchTerm: new FormControl(),
  });

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.people$ = this.route.data.pipe(map((data) => data.people));
  }
}

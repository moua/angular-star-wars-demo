import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Film } from '../film.model';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.scss']
})
export class FilmsComponent implements OnInit {
  public films$: Observable<Film[]>;
  public obj = Object;
  public columnsHeaders = {
    title: 'Title',
    releaseDate: 'Release Date',
    characters: 'Characters',
    starships: 'Starships',
    vehicles: 'Vehicles',
  };
  public routes = {
    characters: '/characters/character-details',
    starships: '/starships/starship-details',
    vehicles: '/automobile/vehicle-details',
  };
  public searchForm = new FormGroup({
    searchTerm: new FormControl(),
  });

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.films$ = this.route.data.pipe(map((data) => data.films));
  }

  /**
   * Task for returning the episode id to be tracked.
   * @param index the index of the item.
   * @param film the film object.
   * @returns the episode id of the film.
   */
  public trackEpisodeId(index: number, film: Film): number {
    return film.episodeId;
  }
}

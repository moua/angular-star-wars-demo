import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Vehicle } from 'src/app/vehicles.model';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {
  public vehicles$: Observable<Vehicle[]>;
  public columnsHeaders = {
    name: 'Name',
    model: 'Model',
    manufacturer: 'Manufacturer',
    vehicleClass: 'Class',
    url: 'Details',
  };
  public searchForm = new FormGroup({
    searchTerm: new FormControl(),
  });

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    console.log('init');
    this.vehicles$ = this.route.data.pipe(map((data) => data.vehicles));
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesResolver } from '../resolvers/vehicles.resolver';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { SharedModule } from '../shared/shared.module';
import { VehicleDetailsComponent } from './vehicle-details/vehicle-details.component';
import { MatCardModule } from '@angular/material/card';
import { VehicleDetailsResolver } from '../resolvers/vehicle-details.resolver';

const routes: Routes = [
  {
    path: '',
    component: VehiclesComponent,
    data: { title: 'Vehicles'},
    resolve: { vehicles: VehiclesResolver },
  },
  {
    path: 'vehicle-details/:id',
    component: VehicleDetailsComponent,
    data: { title: 'Vehicle Details'},
    resolve: { vehicle: VehicleDetailsResolver },
  },
];

@NgModule({
  declarations: [
    VehiclesComponent,
    VehicleDetailsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
  ]
})
export class AutomobileModule { }

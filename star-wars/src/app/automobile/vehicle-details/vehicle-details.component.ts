import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Vehicle } from 'src/app/vehicles.model';

@Component({
  selector: 'app-vehicle-details',
  templateUrl: './vehicle-details.component.html',
  styleUrls: ['./vehicle-details.component.scss']
})
export class VehicleDetailsComponent implements OnInit {
  public vehicle$: Observable<Vehicle>;
  public obj = Object;
  public labels = {
    name: 'Name',
    model: 'Model',
    manufacturer: 'Manufacturer',
    costInCredits: 'Cost In Credits',
    length: 'Length',
    maxAtmospheringSpeed: 'Max Atmosphering Speed',
    crew: 'Crew',
    passengers: 'Passengers',
    cargoCapacity: 'Cargo Capacity',
    consumables: 'Comsumables',
    vehicleClass: 'Starship Class',
    pilots: 'Pilots',
    films: 'Films',
    created: 'Created',
    edited: 'Edited',
    url: 'URL',
  };

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.vehicle$ = this.route.data.pipe(map((data) => data.vehicle));
  }
}
